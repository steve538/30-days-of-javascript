const style = document.documentElement.style;

   var color = document.getElementById('colorInput');
   var blur = document.getElementById('blurRange');
   var spacing = document.getElementById('spacingRange');

function setNewColor(){
    style.setProperty('--backgroundColor',color.value);
}

function setNewSpacing(){
    style.setProperty('--spacing',spacing.value+'px');
}

function blurPicture(){
    style.setProperty('--blur',blur.value+'px');
}
