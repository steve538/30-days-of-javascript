console.log("Działa!");

document.onkeydown = function(event){
    var keyCode = window.event.keyCode;
    var selectedKey ="";
    if(keyCode == 65){
        document.getElementById('Bongo').play();
        selectedKey = "A";
    }else if(keyCode == 83){
        document.getElementById('Claps').play();
        selectedKey = "S";
    }else if(keyCode == 68){
        document.getElementById('Congo').play();
        selectedKey = "D";
    }else if(keyCode == 70){
        document.getElementById('Electronic').play();
        selectedKey = "F";
    }else if(keyCode == 71){
        document.getElementById('House').play();
        selectedKey = "G";
    }else if(keyCode == 72){
        document.getElementById('Maracas').play();
        selectedKey = "H";
    }else if(keyCode == 74){
        document.getElementById('Scratch').play();
        selectedKey = "J";
    }else if(keyCode == 75){
        document.getElementById('Rings').play();
        selectedKey = "K";
    }

    if(selectedKey != ""){
        var selectedElement = document.getElementById(selectedKey);
        var prevColor = selectedElement.style.backgroundColor;
        selectedElement.style.backgroundColor = "orange";
        setTimeout(function(){
            selectedElement.style.backgroundColor = prevColor;
       }, 500);
    }
}