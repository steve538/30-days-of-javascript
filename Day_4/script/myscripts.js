

function handleClick(){

    const infoTextsArray = 
    ["In this country you shouldn't give any tip. The staff is usually well paid and extra money could"+
    " confuse or even offend waiter.","In this country tip is already added to the bill. The staff is also"+
    " well paid and doesn't expect and tip. If you really want ,add 4 or 5 dollars to your bill.","In this"+
    " country tip is not obligatory, but you should leave it. We added to your bill extra 10%.","In this"+
    " country tip is MANDATORY. In many cases the staff suggest you size of tip, but usually it's 15% or"+
    " more. We added to your bill extra 15%."];

    var country = document.getElementById("countryList").value;
    var numberOfPeople = document.getElementById("peopleNumber").value;
    var billValue = document.getElementById("billValue").value;
    billValue = Math.round(billValue * 100) / 100;

    var info;
    var tipProcent;

    switch(country){
        case "0":
        info = infoTextsArray[0];
        tipProcent = 0;
        break;
        case "1":
        info = infoTextsArray[1];
        tipProcent = 0;
        break;
        case "2":
        info = infoTextsArray[2];
        tipProcent = 10;
        break;
        case "3":
        info = infoTextsArray[3];
        tipProcent = 15;
        break;
    }

    var moneyPerPerson = function (){
        var tipValue = 0;
        if(tipProcent != 0){
            tipValue = (billValue/100)*tipProcent;
        }
        tipValue += billValue;

        return "Dollars per person: "+ Math.ceil(tipValue/numberOfPeople);
    }();

    document.getElementById("moneyPerPerson").textContent = moneyPerPerson;
    document.getElementById("tipHelper").textContent = info;

    return false;
}



