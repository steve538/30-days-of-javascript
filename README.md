﻿Ten projekt to próba wzięcia udziału w #JavaScript30 zainicjowanego na tej stronie : https://javascript30.com/.
Z samego kursu biorę tylko pomysł, implementacje programu staram się wykonać sam aby był jak najbardziej zbliżony do prezentowanego oryginału.

1. Dzień pierwszy : Zestaw instrumentów perkusyjnych. Po wciśnieciu odpowiedniego klawisza przez sekundę wydawany jest wskazany dźwięk a sam klawisz zmienia na chwilę kolor.

2. Dzień drugi: Prosty zegar analogowy. Za pomocą JavaScriptu ustawiam początkowe położenie wskazówek a same wskazówki poruszają się za pomocą CSS.
