const HOURS_HAND = document.getElementById("hour-main");
const MINUTES_HAND = document.getElementById("minutes-main");
const SECONDS_HAND = document.getElementById("seconds-main");

window.onload = function() {
    setStartPosition();
  };

function setStartPosition(){
    let data = new Date();
    let currentHour = data.getHours();
    let currentMinutes = data.getMinutes();
    let currentSeconds = data.getSeconds();

    let hourAngle = currentHour * 360/12 + ((currentMinutes * 360/60) / 12);
    let minutesAngle = (currentMinutes * 360/60) + (currentSeconds * 360/60) / 60;
    let secondsAngle = currentSeconds * 360/60;

    HOURS_HAND.style.transform = "rotate(" + hourAngle + "deg)";
    MINUTES_HAND.style.transform = "rotate(" + minutesAngle + "deg)";
    SECONDS_HAND.style.transform = "rotate(" + secondsAngle + "deg)";
}